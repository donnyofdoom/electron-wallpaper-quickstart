# electron-wallpaper-quickstart

A simple framework to get you started building a dynamic desktop for windows.  
~~Sky's~~ **Web animation's** are the limit!

Heavily based off the work of [shundroid](https://github.com/shundroid/wallpaper).  
Redone so it can work out of the box and with documentation.

# Installation
Note that you'll need a functioning version of node-gyp  
`npm install -g --production windows-build-tools`  
`npm install -g node-gyp`  


`git clone https://donnyofdoom@bitbucket.org/donnyofdoom/electron-wallpaper-quickstart.git`  
`cd electron-wallpaper-quickstart`  
`npm i`  
`npm start`

And you should be good to go :)
